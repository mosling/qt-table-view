#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "parametermodel.h"
#include <QMainWindow>
#include <QSortFilterProxyModel>


QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    ParameterModel m_model;
    QSortFilterProxyModel m_proxy;
};
#endif // MAINWINDOW_H
