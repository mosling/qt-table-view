#ifndef PARAMETER_H
#define PARAMETER_H

#include <QString>

class Parameter
{

public:
    Parameter(const QString &category, const QString &name, const QString &value);

    QString category() const { return p_category; }
    QString name() const { return p_name; }
    QString value() const { return p_value; }

private:
    QString p_category;
    QString p_name;
    QString p_value;
};

#endif // PARAMETER_H
