#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    m_model.append({"Set", "tbm", "C:\\tbm"});
    m_model.append({"Set", "name", "Model-Generator"});
    m_model.append({"Db", "system", "inetz"});
    m_model.append({"Query", "Objektklasse", "AW projekte"});

    m_proxy.setSourceModel(&m_model);
    m_proxy.setFilterKeyColumn(2);

    ui->tableView->setModel(&m_proxy);
}

MainWindow::~MainWindow()
{
    delete ui;
}

