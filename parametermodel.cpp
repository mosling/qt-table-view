#include "parametermodel.h"

ParameterModel::ParameterModel(QObject *parent)
    : QAbstractTableModel{parent}
{

}

int ParameterModel::rowCount(const QModelIndex &) const
{
    return m_data.count();
}

int ParameterModel::columnCount(const QModelIndex &) const
{
    return 3;
}

QVariant ParameterModel::data(const QModelIndex &index, int role) const
{
   if (role != Qt::DisplayRole && role != Qt::EditRole) return {};
   const auto & parameter = m_data[index.row()];
   switch (index.column()) {
   case 0: return parameter.category();
   case 1: return parameter.name();
   case 2: return parameter.value();
   default: return {};
   };
}

QVariant ParameterModel::headerData(int section, Qt::Orientation orientation, int role) const
{
   if (orientation != Qt::Horizontal || role != Qt::DisplayRole) return {};
   switch (section) {
   case 0: return "Category";
   case 1: return "Name";
   case 2: return "Value";
   default: return {};
   }
}

void ParameterModel::append(const Parameter & parameter)
{
   beginInsertRows({}, m_data.count(), m_data.count());
   m_data.append(parameter);
   endInsertRows();
}
