#ifndef PARAMETERMODEL_H
#define PARAMETERMODEL_H

#include "parameter.h"
#include <QAbstractTableModel>

class ParameterModel : public QAbstractTableModel
{
public:
    explicit ParameterModel(QObject *parent = nullptr);

    int rowCount(const QModelIndex &) const override;
    int columnCount(const QModelIndex &) const override;
    QVariant data(const QModelIndex &index, int role) const override;
    QVariant headerData(int section, Qt::Orientation orientation, int role) const override;

    void append(const Parameter & parameter);

private:
    QList<Parameter> m_data;
};

#endif // PARAMETERMODEL_H
